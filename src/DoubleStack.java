import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DoubleStack {

    private List<Object> stack = new ArrayList<>();

    public static void main(String[] argum) throws CloneNotSupportedException {
        System.out.println(interpret("2 5 SWAP -"));
        System.out.println(interpret("2 5 9 ROT - +"));
        System.out.println(interpret("2 5 9 ROT + SWAP -"));

//        System.out.println(interpret(""));
//        System.out.println(interpret(null));
//        System.out.println(interpret("    "));
//        System.out.println(interpret("2 3 + x"));
//        System.out.println(interpret("2 5 + 8"));
//        System.out.println(interpret("2 - 3"));

    }


    DoubleStack() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {  // Inspiration: java.util.Vector<E>
        // New DoubleStack to Return
        DoubleStack newDoubleStack = new DoubleStack();
        // Copy the stack values
        newDoubleStack.stack = new ArrayList<>(this.stack);
        return newDoubleStack;
    }

    public boolean stEmpty() {
        return size() == 0;
    }

    // Check the size of stack in DoubleStack
    private int size() {
        return stack.size();
    }

    public void push(double a) {
        stack.add(a);
    }

    public double pop() {
        if (size() < 1) {
            throw new IndexOutOfBoundsException("The stack is empty: pop() method is unsable");
        }

        // Get the Object
        Object a = stack.get(stack.size() - 1);
        // Remove the object from stack
        stack.remove(size() - 1);
        // Return the saved copy of object
        return (double) a;
    } // pop


    public void op(String s) {
        if (size() < 2) {
            throw new IndexOutOfBoundsException();
        }
        double top1 = pop();
        double top2 = pop();
        switch (s) {
            case "+":
                push(top1 + top2);
                break;
            case "-":
                push(top2 - top1);
                break;
            case "*":
                push(top1 * top2);
                break;
            case "/":
                push(top2 / top1);
                break;
            default:
                throw new RuntimeException("The symbol is incorrect.");
        }
    }

    public double tos() {
        if (size() < 1) {
            throw new IndexOutOfBoundsException("The stack is empty: tos() method is unusable");
        }
        return (double) stack.get(size() - 1);
    }

    public List<Object> subStack(int i, int N) {
        // Create a subStack:
        // i - index for beginning of sub
        // N - the last object to be in sub
        // Used in method equals()
        return stack.subList(i, N);
    }


    @Override
    public boolean equals(Object o) {
        // Recursion method
        if (o instanceof DoubleStack) {
            // Return false if the stacks are of different lenght
            if (((DoubleStack) o).size() != size()) {
                return false;
            }
            // Return true if both stacks are empty
            if ((((DoubleStack) o).stEmpty() && stEmpty())) {
                return true;
            }
            // The last loop
            if (size() == 1) {
                return tos() == ((DoubleStack) o).tos();
            }
            // Recursion
            if (tos() == ((DoubleStack) o).tos()) {
                return stack.subList(0, size() - 1).equals(((DoubleStack) o).subStack(0, size() - 1));
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuffer line = new StringBuffer();
        for (int i = 0; i < size(); i++) {
            line.append(stack.get(i)).append(i < size() - 1 ? ", " : ""); // Add coma if not last
        }
        return line.toString();
    }

    public static double interpret(String pol) {
        if (pol == null || pol.equals("") || pol.trim().equals("")) {
            throw new RuntimeException("The string entered is empty: interpret() method is unusable");
        }

        // Create a DoubleStack to operate
        DoubleStack toInterpret = new DoubleStack();

        // Get the Array for working
        String[] splitted = pol.replaceAll("(\\s+|\t+|\n+|\b+|\f+)", " ").split(" ");

        // The list of valid symbols
        ArrayList<String> symbols = new ArrayList<>(Arrays.asList("+", "-", "*", "/"));

        // Make the calculations
        String lastOp = "";
        try {
            for (String spl : splitted) {
                if (spl.toUpperCase().equals("SWAP")) {

                    lastOp = spl;
                    toInterpret.swap();
                } else if (spl.toUpperCase().equals("ROT")) {

                    lastOp = spl;
                    toInterpret.rot();
                }
                // While spl is a number add to a stack
                // When the symbol is found make the calculations
                else if (isDouble(spl)) {
                    toInterpret.push(Double.parseDouble(spl));
                } else if (symbols.contains(spl)) {
                    lastOp = spl;
                    toInterpret.op(spl);
                } else {
                    lastOp = spl;
                    throw new RuntimeException(String.format("Invalid operator found: %1$s in %2$s", lastOp, pol));

                }
            }
        } catch (IndexOutOfBoundsException i) {
            throw new IndexOutOfBoundsException(String.format("Not enough numbers for %1$s opration in expression %2$s", lastOp, pol));
        }
        // Can be only 1 Object in the stack
        if (toInterpret.size() > 1) {
            throw new RuntimeException("The string entered has an error. There is an extra symbol in " + pol);
        }
        return toInterpret.tos();
    }

    private void swap() {
        if (stack.size() < 2) throw new IndexOutOfBoundsException();
        double l1 = pop();
        double l2 = pop();
        push(l1);
        push(l2);
    }

    private void rot() {
        if (stack.size() < 3) throw new IndexOutOfBoundsException();
        double l1 = pop();
        double l2 = pop();
        double l3 = pop();
        push(l2);
        push(l1);
        push(l3);
    }

    // Check is the String can be boolean
    private static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}

